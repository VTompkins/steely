# README.md

## Repo Structure 
``` 
./Steely
  |--- data
  |    |-- dataset_geojsons
  |    |     |-- experiment_geojsons
  |    |     :
  |    |     
  |    |-- dataset_yamls
  |    |     :
  |    |     
  |    `-- results
  |          |-- 11_class_STEELY
  |          `-- TEL_ONLY
  |
  |-- src
  |    |-- notebooks
  |    `-- python
  |
  `--- models
       |-- 11_class_STEELY
       `-- TEL_ONLY
```

## Computing Environment (<kraken or octo>/data/steely)
Experimentation, including training and inference, for the Steely project should be completed in the `steely` docker container. The container has access to volumes: `/data, /data2, and /data3`, but the project directory is located in **`/data/steely/`**. The python venv `steely_env` is located within this directory and has been configured for the latest version of yolov5 as of **May 5, 2021**, and has both Quixotic and Drago installed in it. `aws-cli` has also been installed, feel free to setup on profile and setting my profile can be done using the line from the `example_cmds.txt (Line 14)`.

### Spin-up the Docker Container (kraken/data/steely)
`docker exec -it steely /bin/bash`

### Activate Virtual Environment (kraken/data/steely)
`source /data/steely/venv/steely_kraken_env/bin/activate`

### Dataset YAML files for training in Yolov5 (kraken/data/steely)
Panchromatic `allMilVeh_768`: `pan_dataset.yml`

RGB `allMilVeh_768`: `steely_dataset.yml`

Steely LFE `TEL_ONLY`: `tel_only_128_dataset.yml`

Steely Multiclass `11_class_STEELY`: `dataset_11_cat_pre_qc.yml` 

## Ultralytics and Yolov5 Notes
The dataset directories in `/data/steely/` for training the `allMilVeh_768` are `/pan` for the panchromatic images and `/dataset` for the RGB images. Typical Yolov5 directory setup used for training is as follows:
``` 
./dataset
|-- images
|   |-- test
|   |   |
|   |   `-- img01.png
|   |-- train
|   `-- val
`-- labels
    |-- test
	|   |
	|   `-- img01.txt
    |-- train
    `-- val
```

The `/yolov5` directory is the most recent clone from ultralytics and the `/yolov5_old/yolov5` is a previous version. To use Yolov5 you **MUST** work from the yolov5 directory unless the model is loaded from `PyTorch Hub`. 
