# Descriptions of the Models 
```
  `--- models
       |-- 11_class_STEELY
       `-- TEL_ONLY
```

# models
Contains the subdirectories for each major phase of the model development for the military portion of the Steely project (the multiclass and TEL single class models). Each subdirectory contains the yolov5 options, the hyperparameters used for training, the best and last weights of the trained model, and the `gbdxm` packaged model.