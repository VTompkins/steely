from shapely import wkt
import geopandas as gpd
import argparse
# quixotic needs py36 
# ========================


def convert2shapely(_args):
    _fname = _args.src
    _new_fname = _args.dest+"."+_args.driver.lower() #'C:\\Users\\vrt19\\Documents\\Work\\Steely\\converted_test_tel_11_cat.geojson'
    my_df = gpd.read_file(_fname)
    my_df = my_df[my_df["geom"].notna()].copy(deep=True)
    if my_df['geometry'].isnull().values.all(): my_df.drop(labels='geometry', axis='columns', inplace=True)
    # Try for GeoPandas 0.8 or older, except for 
    my_df['geometry'] = gpd.GeoSeries.from_wkt(my_df['geom']) if int(gpd.__version__.split(".")[-2]) > 8 else  my_df['geom'].apply(wkt.loads)
    my_df.drop(colums=["geom"], inplace=true)
    my_geo_df = gpd.GeoDataFrame(my_df, geometry='geometry')
    my_geo_df.to_file(_new_fname, driver=_args.driver)
# =============================


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    req_parser = parser.add_argument_group("required arguments")
    req_parser.add_argument("-s","--src", type=str, help="The FILE to with shapes/polygons to be converted", required=True)
    req_parser.add_argument("-d","--dest", type=str, default="converted_manifest", help="The name (and PATH) to save the converted FILE to", required=True)
    parser.add_argument("--driver", default="GeoJSON", type=str, help="The FIONA driver used to save the coverted FILE")
    _arguments = parser.parse_args()
    convert2shapely(_arguments)
    