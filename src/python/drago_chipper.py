from drago import VectorFileReader, SourceImages, Dataset

#============================================================================================================
#============================================================================================================
# Chip the dataset
#============================================================================================================
#============================================================================================================

test_file = 'C:/Users/vrt19/Documents/Work/Steely/converted_test_tel_11_cat.geojson'
train_file = 'C:/Users/vrt19/Documents/Work/Steely/train_11_pre_qc_converted.geojson'
output_path = 'E:/steely/tel_chipping/train_11_cat_pre_qc_dataset_512_510_step'
output_test_path = 'E:/steely/tel_chipping/test_11_dataset_512_510_step'
source_images = 'E:/steely/tel-labeling'
label_formats = ['InstanceChannelMask', 'Darknet']
chip_size = 512
chip_step = 510 #chip_size / 2

# validation_areas = VectorFileReader(validation_file).read(mode='val')
train_objects = VectorFileReader(train_file).read()
test_objects =VectorFileReader(test_file).read()
phase2_qrpan_images = SourceImages.find(source_images)
print('Sources found: ' + str(len(phase2_qrpan_images)))
ds_train = Dataset(output_path, sources=phase2_qrpan_images)
ds_test = Dataset(output_test_path, sources=phase2_qrpan_images)

# Plan the dataset
#ds.plan(objects, chip_size, chip_step, include_areas=production_grids, validation_areas=validation_areas)
ds_test.plan(test_objects, chip_size, chip_step, include_areas=None, validation_areas=None)

# Save image chips
ds_test.chip(chip_ext='png')  # , image_transform=scale_image)

# Save the labels
ds_test.label(label_formats)


#ds.plan(objects, chip_size, chip_step, include_areas=production_grids, validation_areas=validation_areas)
ds_train.plan(train_objects, chip_size, chip_step, include_areas=None, validation_areas=None)

# Save image chips
ds_train.chip(chip_ext='png')  # , image_transform=scale_image)

# Save the labels
ds_train.label(label_formats)

# TODO Refactor so that it can run from command line with minimal source code editing
# def drago_chipper(_args):    
    # test_file = #'C:/Users/vrt19/Documents/Work/Steely/converted_test_tel_062921.geojson'
    # train_file = #'C:/Users/vrt19/Documents/Work/Steely/converted_train_tel_062921.geojson'
    # output_path = #'E:/steely/tel_chipping/train_dataset_512_500_step'
    # output_test_path = #'E:/steely/tel_chipping/test_dataset_512_500 _step'
    # source_images = #'E:/steely/tel-labeling'
    # label_formats = ['InstanceChannelMask', 'Darknet']
    # chip_size = 512
    # chip_step = 500 #chip_size / 2

    # # validation_areas = VectorFileReader(validation_file).read(mode='val')
    # train_objects = VectorFileReader(train_file).read()
    # test_objects =VectorFileReader(test_file).read()
    # phase2_qrpan_images = SourceImages.find(source_images)
    # print('Sources found: ' + str(len(phase2_qrpan_images)))
    # ds_train = Dataset(output_path, sources=phase2_qrpan_images)
    # ds_test = Dataset(output_test_path, sources=phase2_qrpan_images)

    # # Plan the dataset
    # #ds.plan(objects, chip_size, chip_step, include_areas=production_grids, validation_areas=validation_areas)
    # ds_test.plan(test_objects, chip_size, chip_step, include_areas=None, validation_areas=None)

    # # Save image chips
    # ds_test.chip(chip_ext='png')  # , image_transform=scale_image)

    # # Save the labels
    # ds_test.label(label_formats)


    # #ds.plan(objects, chip_size, chip_step, include_areas=production_grids, validation_areas=validation_areas)
    # ds_train.plan(train_objects, chip_size, chip_step, include_areas=None, validation_areas=None)

    # # Save image chips
    # ds_train.chip(chip_ext='png')  # , image_transform=scale_image)

    # # Save the labels
    # ds_train.label(label_formats)
    
    
# if __name__=="__main__":
    # parser = argparse.ArgumentParser()
    # req_parser = parser.add_argument_group("REQUIRED arguments")
    # req_parser.add_argument("--manifest", type=str, help="The FILEPATH of the manifest file for training dataset",required=True)
    # req_parser.add_argument("-s", "--src", type=str, help="The DIRECTORY or SUBDIRECTORY where the image files are located", required=True)
    # req_parser.add_argument("--size", type=int, default=512, help="The size of tiles after chipping (size x size ) in pixels", required=True)
    # parser.add_argument("--step-size", type=int, default=256, help="The step size or number of overlapping pixels")
    # parser.add_argument("-d", "--dest", type=str, default="chip_results", help="Where DRAGO chipping results are saved")
    # parser.add_argument("--test-manifest", type=str, default=None, help="The FILEPATH of the manifest file for test dataset")
    # parser.add_argument("--validation-area", type=str, default=None, help="The FILEPATH of the validation area")
    # parser.add_argument("--image-ext", type=str, default=".png", help="The EXTENSION for saving the chip results")
    # _arguments = parser.parser_args()
    # drago_chipper(_arguments)
