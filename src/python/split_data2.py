import numpy as np
import glob
import os
import shutil
import random
from itertools import product
import argparse
import warnings


def compare_lists(_list1, _list2):
    _answer = len(_list1) == len(_list2)
    return _answer


def split_data2(_args):
    assert os.path.isdir(_args.src), "SOURCE {} was not found!".format(_args.src)

    _lbl_dir = os.path.join(_args.src, "labels")
    assert os.path.isdir(_lbl_dir), "Expected LABELS to be in {}, but path does not exist...".format(_lbl_dir)

    _img_dir = os.path.join(_args.src, "images")
    assert os.path.isdir(_img_dir), "Expected IMAGES to be in {}, but path does not exist...".format(_img_dir)

    _cp_root = os.path.join(_args.src, _args.dest)
    os.makedirs(_cp_root, exist_ok=True)

    _files = glob.glob(_lbl_dir + "/*" + _args.labels_ext) if _args.use_labels else glob.glob(
        _img_dir + "*" + _args.images_ext)

    if _args.size_check:
        _files = [_f for _f in _files if os.stat(_f).st_size > 0]

    _tiles = [os.path.split(_f)[1].split(".")[0] for _f in _files]
    assert len(_files) == len(_tiles), "Something went HORRIBLY wrong, check source files!!!"

    if len(glob.glob(_lbl_dir + "*" + _args.labels_ext)) == len(glob.glob(_img_dir + "*" + _args.images_ext)):
        warnings.warn("WARNING Number of IMAGES and LABELS are NOT equal...")

    _img_ext = _args.images_ext
    _lbl_ext = _args.labels_ext
    _no_of_samples = len(_tiles)
    _no_of_train = int(np.floor(_args.train_per * _no_of_samples))
    _no_val_sample = int(np.floor(_args.val_per * _no_of_samples))
    _train_samples = random.sample(_tiles, _no_of_train)
    _remaining_samples = set(_tiles) - set(_train_samples)
    _val_samples = list(random.sample(_remaining_samples, _no_val_sample))
    _test_samples = None
    [os.makedirs(os.path.join(_cp_root, _fold, _src_type), exist_ok=True) for _fold, _src_type in
     product(["train", "val"], ["images", "labels"])]

    if _args.test_set:
        assert _args.train_per + _args.val_per < 1, "Train and validation split sizes are 1 or greater..."
        _test_samples = list(_remaining_samples - set(_val_samples))
        [os.makedirs(os.path.join(_cp_root, "test", _src_type)) for _src_type in ["images", "labels"]]

    if _args.no_copy:
        # TODO [] Add check to make sure that both the label and image tile exists
        # for _training_sample, _validation_sample in zip(_train_samples, _val_samples):
        [(shutil.move(os.path.join(_img_dir, _sample + _img_ext), os.path.join(_cp_root, "train", "image")),
          shutil.move(os.path.join(_lbl_dir, _sample + _lbl_ext), os.path.join(_cp_root, "train", "labels")))
         for _sample in _train_samples]
        [(shutil.move(os.path.join(_img_dir, _sample + _img_ext), os.path.join(_cp_root, "val", "image")),
          shutil.move(os.path.join(_lbl_dir, _sample + _lbl_ext), os.path.join(_cp_root, "val", "labels")))
         for _sample in _val_samples]
        if _args.test_set:
            [(shutil.move(os.path.join(_img_dir, _sample + _img_ext), os.path.join(_cp_root, "test", "image")),
              shutil.move(os.path.join(_lbl_dir, _sample + _lbl_ext), os.path.join(_cp_root, "test", "labels")))
             for _sample in _test_samples]
    else:
        [(shutil.copy(os.path.join(_img_dir, _sample + _img_ext), os.path.join(_cp_root, "train", "image")),
          shutil.copy(os.path.join(_lbl_dir, _sample + _lbl_ext), os.path.join(_cp_root, "train", "labels")))
         for _sample in _train_samples]
        [(shutil.copy(os.path.join(_img_dir, _sample + _img_ext), os.path.join(_cp_root, "val", "image")),
          shutil.copy(os.path.join(_lbl_dir, _sample + _lbl_ext), os.path.join(_cp_root, "val", "labels")))
         for _sample in _val_samples]
    if _args.test_set:
        [(shutil.copy(os.path.join(_img_dir, _sample + _img_ext), os.path.join(_cp_root, "test", "image")),
          shutil.copy(os.path.join(_lbl_dir, _sample + _lbl_ext), os.path.join(_cp_root, "test", "labels")))
         for _sample in _test_samples]
    print("Data splitting FINISHED!!!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    req_parser = parser.add_argument_group("REQUIRED arguments")
    req_parser.add_argument("-s", "--src", type=str,
                            help="A YOLOv5 compatible directory w/ 'images' and 'labels' dirs")
    parser.add_argument("-d", "--dest", type=str, default=os.path.join(os.getcwd(), "folds"),
                        help="Where the folds will be saved")
    parser.add_argument("--use-labels", action="store_true", help="")
    parser.add_argument("--image-ext", type=str, default=".png", help="")
    parser.add_argument("--label-ext", type=str, default=".txt", help="")
    parser.add_argument("--test-set", action="store_true", help="")
    parser.add_argument("--no-copy", action="store_true", help="")
    parser.add_argument("--train-per", type=float, default=0.8, help="")  
    parser.add_argument("--val-per", type=float, default=0.2, help="")
    parser.add_argument("--size-check", action="store_true", help="")
    _params = parser.parse_args()
    split_data2(_params)
