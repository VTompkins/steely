# Information About the Data Directory
```
  |--- data
  |    |-- dataset_geojsons
  |    |     |-- experiment_geojsons
  |    |     :
  |    |     
  |    |-- dataset_yamls
  |    |     :
  |    |     
  |    `-- results
  |          |-- 11_class_STEELY
             `-- TEL_ONLY
```
## data
Along with the subdirectories are the `drago` output jsons and the `.cache` files used for training and validation.

### dataset_geojsons
Contains the annotation `GeoJSON` files prior to chipping and formatting into YOLOv5 labels

#### experiment_geojsons
Contains the `GeoJSONs` from experimental runs that investigated chipsize and chip step size on the learn process

### results
Contains the training result graphs and ***maybe*** the inference results for the various models

#### 11_class_STEELY
Contains the results for the latest version of the multiclass military Steely data

#### TEL_ONLY
Contains the results for the latest version of the TEL only model that detects upright and stowed TELs.